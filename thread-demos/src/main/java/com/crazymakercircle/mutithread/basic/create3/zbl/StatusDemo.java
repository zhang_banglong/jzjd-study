package com.crazymakercircle.mutithread.basic.create3.zbl;

import com.crazymakercircle.util.Print;
import lombok.SneakyThrows;

/**
 * @author ：weimo
 * @date ：Created in 2025/1/9
 * @description：线程的状态演示
 */

public class StatusDemo {

    static  class StatusDemoThread extends Thread{


        @SneakyThrows
        @Override
        public void run() {
            System.out.println("hello ");
            Print.cfo(getName()+"运行状态="+getState());

            Thread.sleep(5000);
            Print.tco(getName()+"_运行结束！");
        }
    }

    public static void main(String[] args) throws InterruptedException {

        StatusDemoThread statusDemoThread = new StatusDemoThread();

        Print.cfo(statusDemoThread.getName()+"运行状态33="+statusDemoThread.getState());
        statusDemoThread.start();

        Thread.sleep(1000);
        Print.cfo(statusDemoThread.getName()+"运行状态dd="+statusDemoThread.getState());

    }
}
