package com.crazymakercircle.mutithread.basic.create3.zbl;

import com.crazymakercircle.util.Print;
import org.junit.Test;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.crazymakercircle.util.ThreadUtil.sleepSeconds;

/**
 * @author ：weimo
 * @date ：Created in 2025/1/11
 * @description：线程池测试
 * 因为例子中的corePoolSize为1，阻塞队列的大小为100，按
 * 照线程创建的规则，需要等阻塞队列已满，才会为去创建新的线程。例子中加入了5个任务，阻塞
 * 队列大小为4（<100），所以线程池的调度器不会去创建新的线程，后面的4个任务只能等待
 */

public class CreateThreadPoolDemo01 {



    @Test
    public void testThreadPoolExcutor(){
        ThreadPoolExecutor executor=  new ThreadPoolExecutor(1,100,100,TimeUnit.SECONDS,new LinkedBlockingDeque<>(100));

        for (int i=0;i<5;i++){

            final  int taskIndex=i;
            executor.execute(()->{
                Print.tco("taskIndex="+taskIndex);
                try {
                    Thread.sleep(Long.MAX_VALUE);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            });
        }

        while (true){
            //每隔1秒， 输出线程池的工作任务数量 总计的任务数量
            Print.tco("- activeCount="+executor.getActiveCount()+"- taskCount="+executor.getTaskCount());
            sleepSeconds(1);
        }
    }

}
