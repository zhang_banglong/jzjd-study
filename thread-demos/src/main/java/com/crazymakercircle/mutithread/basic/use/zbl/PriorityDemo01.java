package com.crazymakercircle.mutithread.basic.use.zbl;

import com.crazymakercircle.util.Print;

/**
 * @author ：weimo
 * @date ：Created in 2025/1/9
 * @description：线程优先级测试
 */

public class PriorityDemo01 {

    public static  final  int SlEEP_GAP=1000;

    static  class  PrioritySetThread extends Thread{
        static  int threadNo=1;
        public PrioritySetThread(){
            super("thread-"+threadNo);
            threadNo++;
        }
        public  long opportunities=0;
        public void run(){
            for (int i=0;; i++){
                opportunities++;
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {

        PrioritySetThread[] threads = new PrioritySetThread[10];

        for (int i=0;i<threads.length;i++){
            threads[i] =new PrioritySetThread();

            //设置优先级
            threads[i].setPriority(i+1);
        }

        for (int i=0;i<threads.length;i++){
            threads[i].start();
        }

        // 睡眠1s
        Thread.sleep(SlEEP_GAP*2);

        for (int i=0;i<threads.length;i++){
            Print.cfo(threads[i].getName()+"- 优先级为-"+threads[i].getPriority()+" 运算后的值="+threads[i].opportunities);
        }
    }
}
