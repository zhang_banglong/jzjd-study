package com.crazymakercircle.mutithread.basic.zbl;

import com.crazymakercircle.util.Print;

import java.util.concurrent.*;

import static com.crazymakercircle.util.ThreadUtil.getCurThreadName;
import static com.crazymakercircle.util.ThreadUtil.sleepMilliSeconds;
import static com.crazymakercircle.util.ThreadUtil.sleepSeconds;

/**
 * @author ：weimo
 * @date ：Created in 2025/1/8
 * @description：
 */

public class ExecutorsDemo01 {
    private static final int MAX_TURN = 5;
    // 创建包含俺三个线程的线程池
    private static ExecutorService pool=Executors.newFixedThreadPool(3);


    //创建一个没有返回值的线程
    static class  DemoThread implements Runnable{

        @Override
        public void run() {
            //业务执行逻辑
            for (int j=1;j<MAX_TURN;j++){
                Print.cfo(getCurThreadName()+" 运行轮次："+j);
                sleepMilliSeconds(10);
            }
        }
    }

    //创建一个有返回值的线程

    static  class ReturnableTask implements Callable<Long>{


        @Override
        public Long call() throws Exception {

            long startTime = System.currentTimeMillis();
            //业务执行逻辑
            for (int j=1;j<MAX_TURN;j++){
                Print.cfo(getCurThreadName()+" 运行轮次："+j);
                sleepMilliSeconds(10);
            }
           long used= System.currentTimeMillis()-startTime;

            Print.cfo(getCurThreadName()+" 线程运行结束");

            return used;
        }
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ////方法一：执行一个 Runnable类型的target执行目标实例，无返回
        pool.execute(new DemoThread());

        Future<Long> submit = pool.submit(new ReturnableTask());

        Long result=(Long)submit.get();

        Print.cfo("异步任务的指向结果为："+result);


    }
}
