package com.crazymakercircle.demo.zbl;

import com.crazymakercircle.util.Print;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class CreateDemo03 {

    public  static  final int MAX_TURN=5;
    public static  final  int COMPUE_TIMES=100000000;

    static  class  ReturnableTask implements Callable<Long>{

        // 异步执行逻辑
        @Override
        public Long call() throws Exception {

            long l = System.currentTimeMillis();
            Print.cfo("线程运行开始");

            TimeUnit.SECONDS.sleep(1);
            for (int i=0;i< COMPUE_TIMES;i++){
                int j=i*10000;
            }
           long used= System.currentTimeMillis()-l;
            Print.cfo("线程运行结束");
            return used;
        }
    }

    public static void main(String[] args) throws InterruptedException {

        ReturnableTask returnableTask = new ReturnableTask();

        FutureTask<Long> longFutureTask = new FutureTask<>(returnableTask);
        Thread thread = new Thread(longFutureTask, "returnableThread");
        thread.start();
        TimeUnit.SECONDS.sleep(1);

        Print.cfo("主线程执行自己的逻辑");

        for (int i=0;i<COMPUE_TIMES/2;i++){
            int j=i*1000;
        }
        Print.cfo("主线程逻辑执行结束");

        try {
            Print.cfo("线程占用时间："+longFutureTask.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Print.cfo("》》》》》》执行结束");

    }
}
